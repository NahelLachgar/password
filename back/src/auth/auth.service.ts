import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { User } from '../models/user.model';
import { UserService } from '../user/user.service';

import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {

    constructor(private userService: UserService, private jwtService: JwtService
    ) { }

    async validateUser(email: string, password: string): Promise<any> {
        const user = await this.userService.findByEmail(email);
        const passwordMatch = await this.verifyPassword(password, user.password)
        if (user && passwordMatch) {
            const { password, ...result } = user;
            return result;
        }
        await new Promise(resolve => setTimeout(resolve, 1000));
        return null;
    }

    async login(user: User) {
        const payload = { email: user.email, sub: user.id }
        user.token = this.jwtService.sign(payload)
        return user
    }

    async register(email: string, password: string) {
        const hashedPassword = await this.hashPassword(password)
        const user: User = { email: email, password: hashedPassword }
        await this.userService.insertOne(user)
       
        return this.login(user)
    }

    async hashPassword(plainTextPassword: string): Promise<string> {
        const salt = await bcrypt.genSalt(10);
        return await bcrypt.hash(plainTextPassword, salt);
    }

    async verifyPassword(plainTextPassword: string, hash: string) {
        return await bcrypt.compare(plainTextPassword, hash)
    }
}
