import { LocalAuthGuard } from './local-auth.guard';
import { Request, Controller, Post, UseGuards, Body, BadRequestException } from '@nestjs/common';
import { AuthService } from '../auth/auth.service';
import { Public } from './constants';

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) { }

    @UseGuards(LocalAuthGuard)
    @Post('login')
    async login(@Request() req) {
        return this.authService.login(req.user)
    }

    @Public()
    @Post('register')
    async register(@Body('email') email: string,
        @Body('password') password: string,
        @Body('passwordConfirm') passwordConfirm: string) {
        if (password != passwordConfirm) throw new BadRequestException('password and confirmation don\'t match')
        return this.authService.register(email, password)
    }
}
