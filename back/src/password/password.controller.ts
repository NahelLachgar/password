import { PasswordService } from './password.service';
import { Password } from './../models/password.model';
import { Body, Request, Controller, Delete, Get, Param, Post, Put, UseGuards, Query } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('password')
@UseGuards(JwtAuthGuard)
export class PasswordController {

    constructor(private passwordService: PasswordService) { }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getPasswords(@Request() req, @Query('clear') clear): Promise<Password[]> {
        const passwords = await this.passwordService.findUserPasswords(req.user, clear == 'true')
        return passwords
    }

    @UseGuards(JwtAuthGuard)
    @Post()
    insertPassword(@Request() req, @Body() password: Password) {
        password.user = req.user
        return this.passwordService.insertOne(req.user.password, password)
    }

    @UseGuards(JwtAuthGuard)
    @Put()
    updatePassword(@Body() password: Password) {
        return this.passwordService.update(password)
    }

    @UseGuards(JwtAuthGuard)
    @Delete(':passwordId')
    deletePassword(@Param('passwordId') id: number) {
        return this.passwordService.remove(id)
    }
}
