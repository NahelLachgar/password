import * as jwt from 'jsonwebtoken';
import { Password } from './../models/password.model';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { User } from 'src/models/user.model';

@Injectable()
export class PasswordService {

    constructor(
        @InjectRepository(Password)
        private passwordRepository: Repository<Password>
    ) { }

    async findUserPasswords(user: User, clear: boolean) {
        const passwords = await this.passwordRepository.find({
            where: {
                user: {
                    id: user.id
                }
            }
        })
        
        if (clear) {
            passwords.map(p => p.password = this.decodePassword(p.password, user.password))
        }
        
        return passwords
    }

    async remove(id: number): Promise<DeleteResult> {
        return await this.passwordRepository.delete(id);
    }

    insertOne(userPassword: string, password: Password) {
        const encodedPassword = this.encodePassword(userPassword, password.password)
        password.password = encodedPassword
        return this.passwordRepository.insert(password)
    }

    update(password: Password) {
        return this.passwordRepository.update(password.id, password)
    }

    encodePassword(userPassword: string, password: string): string {
        return jwt.sign({ password: password }, userPassword)
    }

    decodePassword(password: string, userPassword: string) {
        const decodedPassword: any = jwt.verify(password, userPassword)
        return decodedPassword.password
    }
}
