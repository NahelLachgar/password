import { JwtStrategy } from './../auth/jwt.strategy';
import { AuthModule } from './../auth/auth.module';
import { Password } from './../models/password.model';
import { PasswordService } from './password.service';
import { Module } from '@nestjs/common';
import { PasswordController } from './password.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';

@Module({
    imports: [
        TypeOrmModule.forFeature([Password]),
        ],
    controllers: [PasswordController],
    providers: [PasswordService]
})
export class PasswordModule {}
