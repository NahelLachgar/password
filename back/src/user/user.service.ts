import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../models/user.model';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>
    ) { }

    findByEmail(email: string): Promise<User> {
        return this.userRepository.findOne({email: email});
    }

    findById(id: number): Promise<User> {
        return this.userRepository.findOne(id);
    }

    insertOne(user: User) {
        return this.userRepository.insert(user)
    }
}
