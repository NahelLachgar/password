import { AuthModule } from './auth/auth.module';

import { PasswordModule } from './password/password.module';
import { Password } from './models/password.model';
import { User } from './models/user.model';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'root',
      database: 'password',
      entities: [User, Password],
      synchronize: true,
    }),
    PasswordModule,
    UserModule,
    AuthModule
  ],
  providers: [
  ],
  controllers: []
})
export class AppModule{
}
