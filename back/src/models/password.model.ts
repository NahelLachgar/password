import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm"
import { User } from "./user.model"

@Entity()
export class Password {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    domain: string

    @Column()
    email?: string

    @Column()
    password: string

    @Column()
    username?: string

    @ManyToOne(type => User, user => user.id)
    user: User
}