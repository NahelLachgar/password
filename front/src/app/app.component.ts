import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'password';

  constructor(private authService: AuthService, private router: Router) {}

  logout() {
    this.authService.logout()
    this.router.navigateByUrl('/login')
  }
}
