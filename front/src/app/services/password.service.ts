import { AuthService } from './auth.service';
import { environment } from './../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Password } from '../models/password.model';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private authService: AuthService) { }

  getPasswords(clear = false): Observable<Password[]> {
    return this.http.get<Password[]>(`${this.apiUrl}/password?clear=${clear}`)
  }

  insertPassword(password: Password): Observable<Password[]> {
    return this.http.post<Password[]>(`${this.apiUrl}/password`, password)
  }

  updatePassword(password: Password): Observable<Password[]> {
    password.user = this.authService.currentUserValue
    return this.http.put<Password[]>(`${this.apiUrl}/password`, password)
  }

  deletePassword(id: number) {
    return this.http.delete(`${this.apiUrl}/password/${id}`)
  }
}
