import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient,
    private router: Router) {

    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public set currentUserValue(user: User) {
    console.log(user)
    this.currentUserSubject.next(user);
    localStorage.setItem('currentUser', JSON.stringify(user));
  }


  login(email: string, password: string) {
    return this.http.post<User>(
      `${this.apiUrl}/auth/login`, { email: email, password: password })
      .pipe(map(user => {
        this.currentUserValue = user
      }))
  }

  register(email: string, password: string, passwordConfirm: string) {
    return this.http.post<any>(`${this.apiUrl}/auth/register`, { email, password, passwordConfirm }).pipe(map(
      data => console.log(data)
    ))
  }

  logout() {
    this.currentUserValue = null;
    this.router.navigate(['/login']);
  }
}
