import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup
  get email() {return this.form.get('email')}
  get password() {return this.form.get('password')}
  get passwordConfirm() {return this.form.get('passwordConfirm')}

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.initForm()
  }

  initForm() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordConfirm: ['', [Validators.required, Validators.minLength(8)]],
    })
  }
  
  onSubmit() {
    if (this.form.invalid || this.password.value != this.passwordConfirm.value) return false

    const val = this.form.value
    this.authService.register(val.email, val.password, val.passwordConfirm)
    .subscribe(user => {
      this.router.navigateByUrl('/home')
    }, err => {
      console.log(err)
    })
  }
}
