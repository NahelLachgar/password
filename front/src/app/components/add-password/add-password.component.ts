import { Password } from './../../models/password.model';
import { PasswordService } from './../../services/password.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-add-password',
  templateUrl: './add-password.component.html',
  styleUrls: ['./add-password.component.scss']
})
export class AddPasswordComponent implements OnInit {

  form: FormGroup

  get domain(){return this.form.get("domain")}
  get username(){return this.form.get("username")}
  get email(){return this.form.get("email")}
  get password(){return this.form.get("password")}

  pwd: Password

  constructor(private formBuilder: FormBuilder,
              private passwordService: PasswordService,
              private router: Router
             ) { }

  ngOnInit() {
    this.pwd = history.state.password
    console.log(this.pwd)
    this.initForm()
  }

  initForm() {
    this.form = this.formBuilder.group({
      domain: [this?.pwd?.domain ?? "", [Validators.required]],
      email: [this?.pwd?.email ?? "", [Validators.required, Validators.email]],
      username: this?.pwd?.username ?? "",
      password: [this?.pwd?.password ?? "", Validators.required],
    })
  }

  onSubmit() {
    if(this.form.invalid) return false
    
    const password = this.form.value
    if (this.pwd) password.id = this.pwd.id

    const observable = this.pwd ? this.passwordService.updatePassword(password) : this.passwordService.insertPassword(password);
    observable.subscribe(res => {
      return this.router.navigateByUrl('/home')
    })
  }

}
