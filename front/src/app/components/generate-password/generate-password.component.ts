import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as RandomPassword from 'secure-random-password'

@Component({
  selector: 'app-generate-password',
  templateUrl: './generate-password.component.html',
  styleUrls: ['./generate-password.component.scss']
})
export class GeneratePasswordComponent implements OnInit {

  
  form: FormGroup
  password: string

  get letters() {return this.form.get('letters')}
  get numbers() {return this.form.get('numbers')}
  get length() {return this.form.get('length')}
  get specialChars() {return this.form.get('specialChars')}

  constructor(private formBuilder: FormBuilder,
             ) { }

  ngOnInit() {
    this.initForm()
    this.generate()
  }

  initForm() {
    this.form = this.formBuilder.group({
      letters: [true, [Validators.required]],
      numbers: [true, Validators.required],
      length: [8, Validators.required],
      specialChars: [true, Validators.required],
    })

    this.form.valueChanges.subscribe(() => this.generate())
  }

  generate() {
    if(this.length.value == 0) { 
      this.password = ''
      return
    }

    let charTypes = ''

    if (this.numbers.value) charTypes += (RandomPassword.digits)
    if (this.letters.value) charTypes += (RandomPassword.upper, RandomPassword.lower)
    if (this.specialChars.value) charTypes += (RandomPassword.symbols)

    this.password = RandomPassword.randomPassword({length: this.length.value, characters: charTypes})
  }

}
