import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup
  get email() {return this.form.get('email')}
  get password() {return this.form.get('password')}
  returnUrl: string

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/"
    if(this.authService.currentUserValue) this.router.navigate([this.returnUrl])
    this.initForm()
  }

  initForm() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    })
  }
  
  onSubmit() {
    this.authService.login(this.email.value, this.password.value)
    .subscribe(user => {
      this.router.navigateByUrl(this.returnUrl)
    }, err => {
      console.log(err)
    })
  }
}
