import { Router } from '@angular/router';
import { PasswordService } from './../../services/password.service';
import { Password } from './../../models/password.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  passwordsList: Password[] = []
  clearPasswords = false

  constructor(private passwordService: PasswordService, private router: Router) { }

  ngOnInit(): void {
    this.getPasswords()
  }

  getPasswords(callback = null, clear = false) {
    this.passwordService.getPasswords(clear).subscribe(
      passwords => {
        this.passwordsList = passwords
        if (callback) callback()
      })
  }

  removePassword(password: Password) {
    return this.passwordService.deletePassword(password.id).subscribe(() => {
      this.getPasswords()
    })
  }

  goToModify(password: Password) {
    console.log(password)
    this.router.navigateByUrl('/add-password', { state: {password: password}});
  }

  showOrHidePasswords() {
    this.getPasswords(() => {
      this.clearPasswords = !this.clearPasswords
    },
    !this.clearPasswords)
    
  }

}
