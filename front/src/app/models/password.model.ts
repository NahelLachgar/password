import { User } from "./user.model"

export class Password {
    id?: number
    domain: string
    email?: string
    password: string
    username?: string
    user: User
}